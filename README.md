## WPezPlugins: Gutenberg Things Done

__Tricks out the WordPress admin CSS for Gutenberg in order to make it more GTD-y.__

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### IMPORTANT 

1) These enhancements focus on a screen size of min-width: 1600px.

2) Only the Add Block pallet is effected, at least for now. 


### FAQ

**1) Why?**

Having to click every time I needed to open the Add Block pallet felt suboptimal. Also, the minimal color scheme didn't help my eye find what it wanted so I changed that a bit as well.    




### TODO

- Apply style to entire Gutenberg UI / UX. 



### CHANGE LOG

- v0.0.1 - 8 March 2019
   
   Hello Guten-world