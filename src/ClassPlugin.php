<?php

namespace GutenbergThingsDone;

if ( ! class_exists('ClassPlugin')){

    class ClassPlugin{

        protected $_str_root;

        public function __construct( $str_root = false ) {

            $this->_str_root = $str_root;

            add_action( 'admin_enqueue_scripts', [ $this, 'gtdCSS' ] );
        }


        public function gtdCSS(){

            $str_ver = 'v20190311-pm852';

            wp_enqueue_style(
                'gutenberg-td',
                $this->_str_root . 'assets/src/css/gutenberg-td.css',
                [ 'wp-edit-blocks' ],
                $str_ver
            );
        }


    }

}