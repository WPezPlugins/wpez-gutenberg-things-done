<?php
/*
Plugin Name: WPezPlugins: Gutenberg Things Done
Plugin URI: https://gitlab.com/WPezPlugins/wpez-gutenberg-things-done
Description: Tricks out the WordPress admin CSS for Gutenberg in order to make it more GTD-y.
Version: 0.0.1
Author: Mark "Chief Alchemist" Simchock for WPezPlugins
Author URI: https://gitlab.com/WPezPlugins
License: GPLv2 or later
Text Domain: wpez-gtd
*/

namespace GutenbergThingsDone;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

$str_php_ver_comp = '5.4.0';

// we reserve the right to use traits :)
if (version_compare(PHP_VERSION, $str_php_ver_comp, '<')) {
    exit(sprintf('Gutenberg Things Done requires PHP ' . esc_html($str_php_ver_comp) . ' or higher. Your WordPress site is using PHP %s.', PHP_VERSION));
}

function plugin(){

    require_once 'src/ClassPlugin.php';

    $str_root = plugin_dir_url( __FILE__ );

    $new_plugin = new ClassPlugin( $str_root);


}
if (is_admin()) {
    plugin();
}